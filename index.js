/*
votequick - a quick and dirty voting app
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

const express = require('express')
const app = express()
const server = require('http').createServer(app)
require('./lib/app-compression')(app)
const io = require('./lib/server-socketio')(server)
server.listen(parseInt(process.env.PORT, 10) || 8080)
require('./lib/app-enforce-ssl')(app)
require('./lib/app-static')(app)
new (require('./lib/votequick'))(io) // eslint-disable-line no-new

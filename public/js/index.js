/*
votequick - a quick and dirty voting app
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
if (window.console && typeof window.console.info === 'function') {
  console.info('votequick - a quick and dirty voting app\nCopyright (C) 2016 Jason Schmidt\n\nThis program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.')
}

/* global ga */
(function () {
  /* global io */
  var socket = window.socket = io(document.location.href)

  // Borrowed from gitdav
  function toggleClass (el, className, add) {
    var classes = Array.prototype.slice.call(el.classList)

    if (classes.indexOf(className) !== -1) {
      if (!add) {
        classes.splice(classes.indexOf(className), 1)
      }
    } else {
      if (add) {
        classes.push(className)
      }
    }

    el.className = classes.join(' ')
  }

  // Borrowed from Mustache
  var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    '\'': '&#39;',
    '/': '&#x2F;'
  }
  function escapeHtml (string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return entityMap[s]
    })
  }

  // scroll to an element
  function scrollTo (el, callback) {
    callback = callback || function () {}

    var TIMEOUT = 5
    var FRACTION = 100

    var count = 0
    var body = document.body
    var scrollTop = body.scrollTop
    var offsetTop = el.offsetTop
    var increment = Math.abs(scrollTop - offsetTop) / FRACTION
    var direction = scrollTop < offsetTop ? 1 : -1

    function next () {
      var diff = Math.abs(offsetTop - body.scrollTop)
      if (diff < increment || count++ > FRACTION) {
        body.scrollTop = offsetTop
        return callback()
      }

      body.scrollTop += increment * direction
      setTimeout(next, TIMEOUT)
    }

    next()
  }

  // Confirm string contains only a number
  function isOnlyNumber (str) {
    return (str - parseFloat(str) + 1) >= 0
  }

  // Find sum of numbers
  function findSum (numbers) {
    return numbers.reduce(function sumReduce (memo, val) { return memo + val }, 0)
  }

  // Find most common number
  function findMode (numbers) {
    var numberCounts = {}
    var greatestFrequency = 0
    var secondGreatestFrequency = 0
    var foundMode

    numbers.forEach(function bumpFrequencyForNumber (number) {
      numberCounts[number] = (numberCounts[number] || 0) + 1

      if (greatestFrequency < numberCounts[number]) {
        greatestFrequency = numberCounts[number]
        foundMode = number
      } else if (secondGreatestFrequency < numberCounts[number]) {
        secondGreatestFrequency = numberCounts[number]
      }
    })

    // tied
    if (greatestFrequency === secondGreatestFrequency) {
      return null
    }

    // no number appears more often than any other
    if (greatestFrequency === 1) {
      return null
    }

    return foundMode
  }

  // Find standard deviation
  function findStdDev (numbers, mean) {
    if (mean === undefined) {
      mean = findSum(numbers) / numbers.length
    }

    var squaredDiffs = numbers.map(function (number) {
      return Math.pow(number - mean, 2)
    })

    var avgSquaredDiff = findSum(squaredDiffs) / numbers.length
    var stddev = Math.sqrt(avgSquaredDiff)

    return stddev
  }

  // Cached because we don't replace them
  var containerEl = document.getElementById('container')
  var loaderEl = document.getElementById('loader')
  var roomNameEl = document.getElementById('roomName')
  var nameEl = document.getElementById('name')
  var questionContainerEl = document.getElementById('question-container')
  var questionEl = document.getElementById('question')
  var editQuestionContainerEl = document.getElementById('edit-question-container')
  var editQuestionEl = document.getElementById('edit-question')
  var voteEl = document.getElementById('vote-value')
  var submitVoteEl = document.getElementById('submit-vote')
  var abstainVoteEl = document.getElementById('abstain-vote')
  var currentQuestionContainerEl = document.getElementById('current-container')
  var countdownEl = document.getElementById('countdown')
  var currentResultsEl = document.getElementById('current-results')
  var pastQuestionsEl = document.getElementById('past-questions')
  var clearHistoryEl = document.getElementById('clear-history')

  socket.on('connect', function () {
    toggleClass(loaderEl, 'hide', true)
    toggleClass(containerEl, 'hide', false)
  })
  socket.on('disconnect', function () {
    toggleClass(loaderEl, 'hide', false)
    toggleClass(containerEl, 'hide', true)
  })

  // Global props
  var roomName = null
  var name = null
  var question = null
  var questionLocked = false
  var vote = null
  var submitted = false
  var roomHistory = null
  var editQuestionConfirmed = false

  function updateMetrics () {
    ga('set', 'metric1', roomName && roomName.length || 0)
    ga('set', 'metric2', name && name.length || 0)
    ga('set', 'metric3', question && question.length || 0)
    ga('set', 'metric4', vote && vote.length || 0)
    ga('set', 'metric5', roomHistory && roomHistory.length || 0)
  }
  updateMetrics()

  // name and roomName officially come from location
  function updateFromHash () {
    if (!document.location.hash) return

    var parts = document.location.hash.replace(/^#/, '').split('/')
    var changed = false
    if (parts[0]) {
      parts[0] = decodeURIComponent(parts[0])
      if (roomName !== parts[0]) {
        changed = true
      }
      roomName = parts[0]
      roomNameEl.value = roomName

      var localStorageNameKey = 'votequick.nameinroom.' + String(roomName).toLowerCase()
      var nameFromLocalStorage = localStorage.getItem(localStorageNameKey)

      var updateName = function updateName (newName) {
        if (name !== newName) {
          changed = true
        }
        name = newName
        nameEl.value = name

        toggleClass(currentQuestionContainerEl, 'hide', false)
        if (changed) {
          scrollTo(currentQuestionContainerEl, function () {
            voteEl.focus()
          })
        }
      }

      if (parts[1]) {
        updateName(decodeURIComponent(parts[1]))
      } else if (nameFromLocalStorage) {
        updateName(nameFromLocalStorage)
      } else if (changed) {
        nameEl.focus()
      }

      updateMetrics()
    } else {
      roomNameEl.focus()
    }

    if (changed) {
      identify()
    }
  }
  function updateFromEl () {
    var roomNameFromEl = roomNameEl.value || null
    var nameFromEl = nameEl.value || null
    var changed = roomNameFromEl !== roomName || nameFromEl !== name
    if (changed) {
      updateMetrics()

      if (roomNameFromEl !== roomName) {
        ga('send', 'event', 'Room', 'Edit')
      }
      if (nameFromEl !== name) {
        ga('send', 'event', 'Name', 'Edit')
      }

      roomName = roomNameFromEl
      name = nameFromEl
      identify()
    }
  }
  function identify () {
    document.title = (roomName ? roomName + ' - ' : '') + 'VoteQuick'

    var hash = '#'
    if (roomName !== null) {
      hash += encodeURIComponent(roomName)

      if (name !== null) {
        hash += '/' + encodeURIComponent(name)
      }
    }
    document.location.replace(hash)

    if (socket.connected) {
      socket.emit('votequick.identify', {
        name: name,
        roomName: roomName
      })
    }

    toggleClass(currentQuestionContainerEl, 'hide', !roomName)

    try {
      roomHistory = JSON.parse(localStorage.getItem('votequick.roomhistory.' + String(roomName).toLowerCase()))
    } catch (e) { }
    roomHistory = roomHistory || []
    updateMetrics()
    restoreHistory()

    if (roomName) {
      var localStorageNameKey = 'votequick.nameinroom.' + String(roomName).toLowerCase()
      localStorage.setItem(localStorageNameKey, name || '')
    }
  }
  roomNameEl.focus()
  window.addEventListener('hashchange', updateFromHash, false)
  roomNameEl.addEventListener('keyup', updateFromEl, false)
  nameEl.addEventListener('keyup', updateFromEl, false)
  updateFromHash()
  socket.on('connect', identify)

  function updateQuestionFromEl () {
    var questionFromEl = questionEl.value || null
    if (questionFromEl !== question) {
      question = questionFromEl
      socket.emit('votequick.question', {
        title: question
      })

      updateMetrics()
      ga('send', 'event', 'CurrentQuestion', 'Edit')
    }
  }
  questionEl.addEventListener('keyup', updateQuestionFromEl, false)

  function updateVotingControls () {
    submitVoteEl.innerText = submitted && vote ? 'Rescind' : 'Vote'
    abstainVoteEl.innerText = submitted && !vote ? 'Rescind' : 'Abstain'

    if (submitted) {
      voteEl.setAttribute('disabled', 'disabled')
    } else {
      voteEl.removeAttribute('disabled')
    }

    if (vote) {
      submitVoteEl.removeAttribute('disabled')
    } else {
      submitVoteEl.setAttribute('disabled', 'disabled')
    }

    if (submitted && vote) {
      abstainVoteEl.setAttribute('disabled', 'disabled')
    } else {
      abstainVoteEl.removeAttribute('disabled')
    }

    toggleClass(submitVoteEl, 'button-primary', !submitted && !!vote)
    toggleClass(abstainVoteEl, 'button-primary', !submitted && !vote)

    updateQuestionControls()
  }

  function updateQuestionControls () {
    toggleClass(questionContainerEl, 'ten', questionLocked)
    toggleClass(questionContainerEl, 'twelve', !questionLocked)
    toggleClass(editQuestionContainerEl, 'hide', !questionLocked)

    if (questionLocked) {
      questionEl.setAttribute('disabled', 'disabled')
    } else {
      questionEl.removeAttribute('disabled')
    }
  }

  editQuestionEl.addEventListener('click', function () {
    var result = editQuestionConfirmed
    if (!result) {
      result = window.confirm('Are you sure you want to edit the question? Submitted votes will be cleared once you begin editing.')
    }

    if (result) {
      editQuestionConfirmed = true
      questionLocked = false
      updateQuestionControls()
      questionEl.focus()
    }
  }, false)

  function sendVote () {
    socket.emit('votequick.vote', {
      value: vote,
      submitted: submitted
    })
  }

  function updateVoteFromEl (e) {
    var voteFromEl = voteEl.value || null
    if (voteFromEl !== vote) {
      vote = voteFromEl
      updateMetrics()
      sendVote()
    }

    updateVotingControls()

    if (e.keyCode === 13) {
      submitVote(e)
    }
  }
  voteEl.addEventListener('keyup', updateVoteFromEl, false)

  function submitVote (e) {
    submitted = !submitted
    sendVote()
    updateVotingControls()

    if (submitted) {
      if (vote) {
        submitVoteEl.focus()
      } else {
        abstainVoteEl.focus()
      }

      ga('send', 'event', 'CurrentQuestion', 'Vote')
    } else {
      // voteEl.focus() would only work for mouse clicks. Keyboard enter
      // key gets captured by the input because keyup fires after click, and
      // the focus change is immediate

      ga('send', 'event', 'CurrentQuestion', 'Rescind')
    }
  }
  function abstainVote (e) {
    vote = null
    voteEl.value = ''
    updateMetrics()
    submitVote(e)
  }
  submitVoteEl.addEventListener('click', submitVote, false)
  abstainVoteEl.addEventListener('click', abstainVote, false)

  window.confirmEject = function (el) {
    if (window.confirm('Are you sure you want to eject ' + el.dataset.voterName + '?')) {
      socket.emit('votequick.eject', {socketId: el.dataset.socketId})
    }
  }

  // This is separate from `formatBallot` because we know the ongoing tally
  // will only have one unicode character for the vote value for each voter, so
  // this is a more efficient layout. If we ever show ongoing votes, using
  // `formatBallot` instead of `formatCurrentBallot` would be better.
  function formatCurrentBallot (votes) {
    var html = ''
    votes.forEach(function (vote) {
      html += escapeHtml(vote.value || '?') + '&nbsp;'
      if (vote.isSelf) {
        html += '<span class="self">'
      }
      html += escapeHtml(vote.name || 'Anonymous')
      if (vote.isSelf) {
        html += '</span>'
      } else {
        html += '<button class="button-tight" onclick="window.confirmEject(this);" data-voter-name="' + encodeURIComponent(vote.name) + '" data-socket-id="' + encodeURIComponent(vote.socketId) + '">eject</button>'
      }
      html += '<br />'
    })
    return html
  }

  function markSelfOnBallotData (data) {
    data.votes.forEach(function (vote) {
      vote.isSelf = Boolean(vote.socketId === socket.id)
    })
  }

  socket.on('votequick.current', function (data) {
    if (data.question.setBy !== socket.id && questionEl.value !== data.question.title) {
      questionEl.value = data.question.title
      question = data.question.title

      updateMetrics()
    }

    if (data.questionLocked !== questionLocked) {
      questionLocked = data.questionLocked
      updateQuestionControls()
    }

    markSelfOnBallotData(data)

    currentResultsEl.innerHTML = formatCurrentBallot(data.votes)
    countdownEl.innerHTML = parseInt(data.countdown, 10) || '&nbsp;'
  })

  socket.on('votequick.resetvotes', function () {
    vote = null
    voteEl.value = ''
    submitted = false

    updateVotingControls()
    updateMetrics()
  })

  function formatBallot (votes) {
    var html = ''
    votes.forEach(function (vote) {
      var selfClass = vote.isSelf ? ' self' : ''
      html += '<div class="row' + selfClass + '">\n'
      html += '<div class="four columns">' + escapeHtml(vote.name || 'Anonymous') + '</div>\n'
      html += '<div class="eight columns">' + escapeHtml(vote.value || 'Abstained') + '</div>\n'
      html += '</div>\n'
    })
    return html
  }

  function analyzeNumbers (votes) {
    var numbers = []
    votes.forEach(function isVoteNumber (vote) {
      if (isOnlyNumber(vote.value)) {
        numbers.push(Number.parseFloat(vote.value))
      }
    })
    if (numbers.length <= 1) {
      return ''
    }

    numbers.sort()

    var count = numbers.length
    var min = Math.min.apply(Math, numbers)
    var max = Math.max.apply(Math, numbers)
    var range = max - min
    var sum = findSum(numbers)
    var mean = sum / count
    var median = numbers[Math.floor(count / 2)]
    var mode = findMode(numbers)
    var stddev = findStdDev(numbers, mean)

    var html = '<p class="statistics">'
    html += 'Found '
    html += count
    html += ' votes which contained only numbers. Of these, the minimum was '
    html += min
    html += ' and the maximum was '
    html += max
    html += ' giving a range of '
    html += range
    html += '. The average value was '
    html += mean
    html += ' with a standard deviation of '
    html += stddev
    html += ' and with the middle number being '
    html += median
    html += '.'
    if (mode !== null) {
      html += ' The most frequent number was '
      html += mode
      html += '.'
    }
    html += '</p>'

    return html
  }

  function isUnanimous (votes) {
    var unanimous = true
    var lastVote = null
    var voteCount = 0

    votes.forEach(function checkIndividualVoteForUnanimous (vote) {
      if (vote.value === null) {
        // skip abstained votes
        return
      }

      if (voteCount && lastVote !== vote.value) {
        unanimous = false
      }

      ++voteCount
      lastVote = vote.value
    })

    return Boolean(voteCount > 1 && unanimous)
  }

  function handleResults (data) {
    question = null
    questionLocked = false
    questionEl.value = ''
    vote = null
    voteEl.value = ''
    voteEl.removeAttribute('disabled')
    submitted = false
    updateVotingControls()
    updateMetrics()

    var html = ''
    if (!pastQuestionsEl.innerHTML) {
      html += '<hr class="hide-print">'
      html += '<h1 class="hide-print">Previous Questions</h1>'
      html += '<div class="hide-print small">Note: History is stored in your browser, not on the server. If you want a permanent record, print the page.</div>'
    }
    if (pastQuestionsEl.innerHTML) {
      html += '<hr class="hide-print">'
    }
    html += '<h2>' + escapeHtml(data.question.title || 'Question') + '</h2>\n'
    html += formatBallot(data.votes)
    if (isUnanimous(data.votes)) {
      html += '<p class="statistics">'
      html += 'The vote was unanimous!'
      html += '</p>'
    } else {
      html += analyzeNumbers(data.votes)
    }

    pastQuestionsEl.innerHTML += html
    toggleClass(clearHistoryEl, 'hide', false)
  }

  function restoreHistory () {
    pastQuestionsEl.innerHTML = ''

    roomHistory.forEach(function (data) {
      handleResults(data)
    })

    toggleClass(clearHistoryEl, 'hide', !roomHistory.length)
  }

  socket.on('votequick.results', function (data) {
    markSelfOnBallotData(data)

    roomHistory.push(data)
    localStorage.setItem('votequick.roomhistory.' + String(roomName).toLowerCase(), JSON.stringify(roomHistory))
    updateMetrics()
    handleResults(data)

    scrollTo(currentQuestionContainerEl, function () {
      voteEl.focus()
    })
  })

  function clearHistory () {
    ga('send', 'event', 'PastQuestions', 'Clear')

    roomHistory = []
    localStorage.removeItem('votequick.roomhistory.' + String(roomName).toLowerCase())
    updateMetrics()
    restoreHistory()
    toggleClass(clearHistoryEl, 'hide', true)
  }
  clearHistoryEl.addEventListener('click', clearHistory, false)

  socket.on('votequick.ejectedBy', function (data) {
    window.alert('You were ejected from the room by ' + data.name)
    roomNameEl.value = ''
    nameEl.value = ''
    updateFromEl()
  })
})()

ga('send', 'pageview')

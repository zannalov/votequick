module.exports = (app) => {
  const compression = require('compression')
  const middleware = compression()
  app.use(middleware)
}

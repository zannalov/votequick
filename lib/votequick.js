/*
votequick - a quick and dirty voting app
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

// Socket.IO based quick voting app

// Event: votequick.identify <- { name , roomName }
// Event: votequick.question <- { title }
// Event: votequick.vote <- { value, submitted }
// Event: votequick.eject <- { socketId }
// Event: votequick.ejectedBy -> { socketId, name }
// Event: votequick.current, votequick.results -> {
//   roomName: String,
//   question: { title },
//   questionLocked: Boolean,
//   votes: [
//     {socketId, name, value}
//   ],
//   countdown: Number
// }
// Event: votequick.resetvotes -> no data

// VoteQuick.io = SocketIO

// VoteQuick.rooms = Map ROOM_KEY: {
//   roomName: String,
//   sockets: Map SOCKET_ID: SocketIO.Socket
//   question: {
//     setBy: SocketIO.Socket.id
//     title: null || String
//   },
//   countdown: null || Number
// }

// socket.votequick = {
//   name: null || String,
//   roomKey: null || String,
//   vote: {
//     value: null || String,
//     submitted: Boolean
//   }
// }

class VoteQuick {
  constructor (io) {
    this.rooms = new Map()
    this.io = io

    io.on('connection', this.onConnection.bind(this))
  }

  onConnection (socket) {
    socket.votequick = {
      name: null,
      roomKey: null
    }

    socket.on('votequick.identify', this.onIdentify.bind(this, socket))
    socket.on('votequick.question', this.onQuestion.bind(this, socket))
    socket.on('votequick.vote', this.onVote.bind(this, socket))
    socket.on('votequick.eject', this.onEject.bind(this, socket))
    socket.on('disconnect', this.onDisconnect.bind(this, socket))

    this.initVote(socket)
  }

  initVote (socket) {
    socket.votequick.vote = {
      value: null,
      submitted: false
    }
  }

  initQuestion (roomKey) {
    this.rooms.get(roomKey).question = {
      title: null
    }

    this.resetVotes(roomKey) // performs a checkRoom for us
  }

  onIdentify (socket, options) {
    let name = options && options.name
    if (typeof name !== 'string' || !name) {
      name = null
    }

    let roomName = options && options.roomName
    let roomKey
    if (typeof roomName !== 'string' || !roomName) {
      roomName = null
      roomKey = null
    } else {
      roomKey = roomName.toLowerCase()
    }

    const roomKeyChanged = socket.votequick.roomKey !== roomKey

    if (socket.votequick.roomKey && roomKeyChanged) {
      this.leaveRoom(socket, socket.votequick.roomKey)
    }

    socket.votequick = {
      name: name,
      roomKey: roomKey
    }

    this.initVote(socket)

    if (roomKey) {
      if (roomKeyChanged) {
        this.initRoom(roomKey, roomName)
        this.enterRoom(socket, roomKey) // performs checkRoom for us
      } else {
        this.rooms.get(roomKey).roomName = roomName
        this.checkRoom(roomKey) // inform room of new name
      }
    }
  }

  initRoom (roomKey, roomName) {
    if (this.rooms.has(roomKey)) return

    this.rooms.set(roomKey, {
      roomName: roomName,
      sockets: new Map(),
      countdown: null
    })

    this.initQuestion(roomKey)
  }

  enterRoom (socket, roomKey) {
    socket.join('votequick.' + roomKey)

    this.rooms.get(roomKey).sockets.set(socket.id, socket)

    this.checkRoom(roomKey)
  }

  leaveRoom (socket, roomKey) {
    socket.leave('votequick.' + roomKey)

    const roomSockets = this.rooms.get(roomKey).sockets
    roomSockets.delete(socket.id)
    if (roomSockets.size) {
      this.checkRoom(roomKey)
    } else {
      this.rooms.delete(roomKey)
    }
  }

  resetVotes (roomKey) {
    this.rooms.get(roomKey).sockets.forEach((roomSocket) => {
      this.initVote(roomSocket)
    })

    this.checkRoom(roomKey)
  }

  onQuestion (socket, options) {
    const roomKey = socket.votequick.roomKey
    const room = roomKey && this.rooms.get(roomKey)
    const question = room && room.question
    if (!question) return

    question.setBy = socket.id
    question.title = options.title

    this.io.to('votequick.' + roomKey).emit('votequick.resetvotes')
    this.resetVotes(roomKey) // performs a checkRoom for us
  }

  onVote (socket, options) {
    if (!socket.votequick) return

    socket.votequick.vote = {
      value: options.value,
      submitted: options.submitted
    }

    this.checkRoom(socket.votequick.roomKey)
  }

  onEject (socket, options) {
    if (!socket.votequick || !socket.votequick.roomKey || !options || !options.socketId) {
      return
    }

    const roomKey = socket.votequick.roomKey
    const room = this.rooms.get(roomKey)
    const socketToEject = room.sockets.get(options.socketId)
    if (!socketToEject) {
      return
    }

    socketToEject.emit('votequick.ejectedBy', {
      socketId: socket.id,
      name: socket.votequick.name
    })

    this.leaveRoom(socketToEject, roomKey)
  }

  onDisconnect (socket) {
    const roomKey = socket.votequick.roomKey
    if (roomKey) {
      this.leaveRoom(socket, roomKey)
    }
  }

  _scheduleCountdown (roomKey) {
    const room = this.rooms.get(roomKey)
    room.countdownHandle = setTimeout(this.checkRoom.bind(this, roomKey), 1000)
  }

  _clearCountdown (room) {
    if (room.countdownHandle) {
      clearTimeout(room.countdownHandle)
      delete room.countdownHandle
    }
  }

  _updateCountdown (room, votingComplete) {
    if (!votingComplete) {
      room.countdown = null
    } else if (room.countdown === null) {
      room.countdown = 5
    } else {
      --room.countdown
    }
  }

  checkRoom (roomKey) {
    const room = this.rooms.get(roomKey)
    if (!room) return

    this._clearCountdown(room)

    const results = {
      roomName: room.roomName,
      question: room.question,
      questionLocked: false,
      votes: []
    }

    let allSubmitted = Boolean(room.sockets.size)
    room.sockets.forEach((socket) => {
      allSubmitted = Boolean(allSubmitted && socket.votequick && socket.votequick.vote && socket.votequick.vote.submitted)
    })

    this._updateCountdown(room, allSubmitted)
    results.countdown = room.countdown

    const countdownComplete = allSubmitted && !room.countdown

    room.sockets.forEach((socket) => {
      const vote = socket.votequick.vote
      const voteValue = vote && vote.value
      const voteSubmitted = vote && vote.submitted

      let maskedVoteValue
      if (voteSubmitted) {
        maskedVoteValue = '\u2713'
        results.questionLocked = true
      } else if (voteValue) {
        maskedVoteValue = '\u270D'
        results.questionLocked = true
      } else {
        maskedVoteValue = '\u23F0'
      }

      results.votes.push({
        socketId: socket.id,
        name: socket.votequick.name,
        value: countdownComplete ? voteValue : maskedVoteValue
      })
    })

    this.io.to('votequick.' + roomKey).emit(countdownComplete ? 'votequick.results' : 'votequick.current', results)

    if (countdownComplete) {
      this.initQuestion(roomKey)
    } else if (allSubmitted) {
      this._scheduleCountdown(roomKey)
    }
  }
}

module.exports = VoteQuick

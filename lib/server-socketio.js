module.exports = (server) => {
  return require('socket.io')(server, {
    pingTimeout: 10000,
    pingInterval: 1000
  })
}

/*
votequick - a quick and dirty voting app
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

const forceSsl = require('express-force-ssl')

function appEnforceSsl (app) {
  if (process.env.NODE_ENV !== 'production') {
    return
  }

  app.set('forceSSLOptions', {
    enable301Redirects: true,
    trustXFPHeader: true,
    httpsPort: 443,
    sslRequiredMessage: 'SSL Required.'
  })

  app.use(forceSsl)
}

module.exports = appEnforceSsl

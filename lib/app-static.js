module.exports = (app) => {
  const express = require('express')
  const path = require('path')
  const staticPath = path.normalize(path.join(__dirname, '..', 'public'))
  const staticOptions = {
    extensions: [
      'html'
    ],
    maxAge: 86400000 /* 1 day in milliseconds */
  }
  const middleware = express.static(staticPath, staticOptions)
  app.use(middleware)
}

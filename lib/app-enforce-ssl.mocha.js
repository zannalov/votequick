/*
votequick - a quick and dirty voting app
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

const expect = require('chai').expect
const sinon = require('sinon')

const appEnforceSsl = require('./app-enforce-ssl')

describe('appEnforceSsl', () => {
  it('should be a function', () => {
    expect(appEnforceSsl, 'appEnforceSsl module output').to.be.a('function')
  })

  it('should not set up the app when not in production', () => {
    const app = {
      set: sinon.stub(),
      use: sinon.stub()
    }

    process.env.NODE_ENV = 'development'

    appEnforceSsl(app)

    expect(app.set.called, 'app.set called').to.be.false
    expect(app.use.called, 'app.use called').to.be.false
  })

  it('should set up the app when in production', () => {
    const app = {
      set: sinon.stub(),
      use: sinon.stub()
    }

    process.env.NODE_ENV = 'production'

    appEnforceSsl(app)

    expect(app.set.calledOnce, 'app.set called exactly once').to.be.true
    expect(app.use.calledOnce, 'app.use called exactly once').to.be.true
  })
})

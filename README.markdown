# votequick
A quick and dirty voting app. See the [help page](https://votequick.herokuapp.com/help) for more information about the purpose and use of the application.

Note: Heroku is no longer offering free dynos, so this has been removed from Heroku. Feel free to fork and upload it if you are willing to support it. Thanks!
